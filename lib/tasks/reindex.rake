# frozen_string_literal: true

$stdout.sync = true

namespace :reindex do
  task :collection, [:collection_id] => :environment do |_t, args|
    puts "Reindexing everything in collection #{args[:collection_id]}"

    query = ActiveFedora::SolrQueryBuilder.construct_query(local_collection_id_ssim: args[:collection_id])

    results = ActiveFedora::SolrService.query(
      query, rows: ActiveFedora::Base.count
    ).map { |hit| SolrDocument.new(hit) }

    failures = []
    results.each do |r|
      begin
        Image.find(r.id).update_index
      rescue => e
        Rails.logger.error e.message
        failures << r.id
      end
    end

    puts "-- FAILED OBJECTS:"
    puts failures
  end
end
