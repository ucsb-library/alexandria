# frozen_string_literal: true

require "local_authority"
require "identifier"

class SolrDocument
  include Blacklight::Solr::Document
  include BlacklightOaiProvider::SolrDocument

  self.timestamp_key = "timestamp"

  include Blacklight::Gallery::OpenseadragonSolrDocument
  include CurationConcerns::SolrDocumentBehavior

  # self.unique_key = 'id'

  # Email uses the semantic field mappings below to generate the body
  # of an email.
  SolrDocument.use_extension(Blacklight::Document::Email)

  # DublinCore uses the semantic field mappings below to assemble an
  # OAI-compliant Dublin Core document Semantic mappings of solr
  # stored fields. Fields may be multi or single valued. See
  # Blacklight::Solr::Document::ExtendableClassMethods#field_semantics
  # and Blacklight::Solr::Document#to_semantic_values Recommendation:
  # Use field names from Dublin Core
  use_extension(Blacklight::Document::DublinCore)

  ##
  # @return [String]
  def to_oai_dpla
    xml = Builder::XmlMarkup.new
    xml.tag!("oai_dc:dc",
             'xmlns:oai_dc' => "http://www.openarchives.org/OAI/2.0/oai_dc/",
             'xmlns:dc' => "http://purl.org/dc/elements/1.1/",
             'xmlns:dct' => "http://purl.org/dc/terms/",
             'xmlns:edm' => "http://www.europeana.eu/schemas/edm/",
             'xmlns:xsi' => "http://www.w3.org/2001/XMLSchema-instance",
             'xsi:schemaLocation' => %(http://www.openarchives.org/OAI/2.0/oai_dc/ http://www.openarchives.org/OAI/2.0/oai_dc.xsd)) do
      to_semantic_values.select { |field, values| dublin_core_field_name? field  }.each do |field,values|
        Array.wrap(values).each { |v| xml.tag! "dc:#{field}", v }
      end

      xml.tag!("edm:object", thumbnail_url)
      xml.tag!("edm:isShownAt", "#{Settings.oai_identifier_prefix}ark:/48907/#{id}")
      xml.tag!("dc:identifier", accession_number)
      xml.tag!("dc:rights", access_policy)

      if self["created_ssm"].present?
        xml.tag!("dct:created", fetch("created_ssm").first)
      end
      if self["issued_ssm"].present?
        xml.tag!("dct:issued", fetch("issued_ssm").first)
      end
    end
    xml.target!
  end

  # Blacklight OAI metadata mapping
  field_semantics.merge!(
    accessPolicy: "isGovernedBy_ssim",
    contributor:  "all_contributors_label_sim",
    coverage:     "location_label_tesim",
    creator:      "creator_label_tesim",
    description:  %w[description_tesim note_label_tesim citation],
    format:       "form_of_work_label_tesim",
    identifier:   "uri_ssm",
    language:     "language_label_ssm",
    publisher:    "publisher_tesim",
    relation:     "collection_label_ssim",
    rights:       "license_tesim",
    subject:      "lc_subject_label_tesim",
    title:        "title_tesim",
    type:         "work_type_label_tesim"
  )

  # Do content negotiation for AF models.
  use_extension(Hydra::ContentNegotiation)

  # This overrides the connection provided by Hydra::ContentNegotiation so we
  # can get the model too.
  module ConnectionWithModel
    def connection
      # TODO: clean the fedora added triples out.
      @connection ||= CleanConnection.new(ActiveFedora.fedora.connection)
    end
  end

  use_extension(ConnectionWithModel)

  # Something besides a local authority
  def curation_concern?
    return false if fetch("has_model_ssim", []).empty?

    CurationConcerns.config.registered_curation_concern_types.any? do |type|
      type == fetch("has_model_ssim", []).first
    end
  end

  def after_embargo_status
    # blank if there is no embargo or the embargo is expired:
    # https://github.com/projecthydra/hydra-head/blob/master/hydra-access-controls/app/models/hydra/access_controls/embargo.rb#L22-L24
    return if self["embargo_release_date_dtsi"].blank?

    date = Date.parse self["embargo_release_date_dtsi"]
    policy = AdminPolicy.find(self["visibility_after_embargo_ssim"].first)
    " - Becomes #{policy} on #{date.to_s(:us)}"
  end

  def etd?
    self["has_model_ssim"] == [ETD.to_rdf_representation]
  end

  def admin_policy_id
    fetch("isGovernedBy_ssim").first
  end

  def to_param
    Identifier.ark_to_noid(ark) || id
  end

  def ark
    Array.wrap(
      self[Solrizer.solr_name("identifier", :displayable)]
    ).first
  end

  # @return [Array<SolrDocument>]
  def file_sets
    return @file_sets unless @file_sets.nil?

    ids = self[Solrizer.solr_name("member_ids", :symbol)]

    @file_sets = if ids.present?
                   query =
                     ActiveFedora::SolrQueryBuilder
                       .construct_query_for_ids(ids)

                   ActiveFedora::SolrService.query(
                     query, rows: FileSet.count
                   ).map { |hit| SolrDocument.new(hit) }
                 else
                   []
                 end
  end

  def accession_number
    Array.wrap(fetch("accession_number_ssim") { [] }).first
  end

  def uri
    Array.wrap(fetch("uri_ssm") { [] }).first
  end

  def thumbnail_url
    path = Array.wrap(fetch("thumbnail_url_ssm") { [] }).first

    return "" if path.blank?

    # many thumbnails are indexed using the smaller 'square' size and
    # region settings. replace this with the 'basic' thumbnail settings.
    basic_region = Settings.thumbnails["basic"]["region"] || 'full'

    url = "https://#{Rails.application.config.host_name}" +
          path
            .sub(Settings.thumbnails["square"]["size"], Settings.thumbnails["basic"]["size"])
            .sub(Settings.thumbnails["square"]["region"], basic_region)

    url
  end

  def access_policy
    Array.wrap(fetch("isGovernedBy_ssim") { [] }).first
  end

  def public_uri
    return nil unless LocalAuthority.local_authority?(self)
    Array.wrap(self["public_uri_ssim"]).first
  end

  # @return [Array<SolrDocument>]
  def in_collections
    query = ActiveFedora::SolrQueryBuilder.construct_query_for_ids(
      fetch("local_collection_id_ssim", [])
    )

    ActiveFedora::SolrService.query(
      query, rows: Collection.count
    ).map { |hit| SolrDocument.new(hit) }
  end

  # @return [Array<SolrDocument>]
  def map_sets
    models = fetch("has_model_ssim", [])

    return [self] if models.include? "MapSet"

    query = if models.include? "ComponentMap"
              { id: self["parent_id_ssim"] }
            else
              {
                index_maps_ssim: self["accession_number_ssim"],
                has_model_ssim: "MapSet",
              }
            end

    # Convert ActiveFedora::SolrHit to SolrDocument
    ActiveFedora::SolrService.query(
      ActiveFedora::SolrQueryBuilder.construct_query(query),
      rows: MapSet.count
    ).map { |hit| SolrDocument.new(hit) }
  end

  # @return [Array<SolrDocument>]
  def index_maps
    field_pairs = fetch("index_maps_ssim", []).map do |index|
      ["accession_number_ssim", index]
    end

    query = ActiveFedora::SolrQueryBuilder.construct_query(field_pairs, " OR ")

    ActiveFedora::SolrService.query(
      query, rows: IndexMap.count
    ).map { |hit| SolrDocument.new(hit) }
  end

  # @return [Array<SolrDocument>]
  def component_maps
    ids = map_sets.map do |set|
      set.fetch("component_maps_ssim", [])
    end.flatten

    batches = []

    while ids.present?
      # Searching for too many IDs at once results in 414 URL Too Long errors
      query = ActiveFedora::SolrQueryBuilder.construct_query_for_ids(
        ids.slice!(0, 250)
      )

      batches += ActiveFedora::SolrService.query(
        query,
        rows: ComponentMap.count
      ).map { |hit| SolrDocument.new(hit) }
    end

    batches.flatten.sort_by { |a| a["accession_number_ssim"] }
  end
end
