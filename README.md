[![Build Status](https://travis-ci.org/ucsblibrary/alexandria.svg?branch=master)](https://travis-ci.org/ucsblibrary/alexandria)

# Developing

1. Copy `config/secrets.yml.template` to `config/secrets.yml` and add
   the LDAP and Ezid passwords from
   [Secret Server](https://epm.ets.ucsb.edu/SS/login.aspx).

1. Start PostgreSQL and create new database and a user with login
   privileges, then add their names to `secrets.yml`.

1. Add the credentials for Active Directory and campus LDAP (from
   Secret Server) to `secrets.yml`.

1. Add the Ezid test password (if you don’t have it, email them for
   it) to `secrets.yml`.

1. Add the SRU base URL (currently Pegasus, soon Alma) to `secrets.yml`.

1. Ensure Java is installed (`brew cask install java`), and start Solr
   and Fedora with `bin/wrap` (stop them with `bin/unwrap`).

1. Run `bundle install` and `CI=1 bin/rake db:migrate`. (The `CI`
   environment variable disables Marmotta, which we don’t need to run
   locally.)

1. Start redis, then a Resque worker with `CI=1 VERBOSE=1 QUEUE='*' INTERVAL=5 bin/rake resque:work`.

1. Start the Rails server with `CI=1 bin/rails s`.

You can run tests with `make spec`, and HTML documentation by
installing `yardoc` with `gem install yardoc` then running `make
html`.

## Docker Environment

A `docker-compose` based development environment is provided. To start the application
and its system dependencies, run `docker-compose up`. ADRL will be available on
http://localhost:3000.

You can get a Rails console within the docker environment with
`docker-compose run web bundle exec rails c`.

# Important environment variables

| Name | Description | Required |
| ---- | ----------- | -------- |
| `ADRL_BINARY_ROOT` | Directory root for local ingest files (defaults to `/opt/ingest` | No |
| `ADRL_DERIVATIVES` | Directory for ADRL to store IIIF derivatives (defaults to `#{Rails.root}/tmp/derivatives`) | No |
| `ADRL_EMAIL` | Email address used as sender for form emails | No |
| `ADRL_MINTER_STATE` | Path to minter statefile (defaults to `#{Rails.root}/tmp/minter-state` | No |
| `ADRL_RIIIF_CACHE` | Directory where RIIIF stores temporary files during derivative creation (defaults to `#{Rails.root}/tmp/network_files`) | No |
| `ADRL_UPLOADS` | Directory where ADRL stores temporary files during upload to Fedora (defaults to `#{Rails.root}/tmp/uploads`) | No |
| `BRANCH_NAME` | Branch name or revision for Capistrano to deploy (defaults to `master`) | No |
| `DATABASE_ADAPTER`| The Rails database adapter to use (defaults to `postgresql`) | Noy |
| `EXPLAIN_PARTIALS` | Debugging variable; when true, prints view information in rendered source | No |
| `FEDORA_BASE` | The base path for Fedora data. | No |
| `FEDORA_PORT` | The port number for the Fedora server (defaults to `8984`) | No |
| `FEDORA_HOST` | The hostname for the Fedora server (defaults to `localhost`) | No |
| `FFMPEG_PATH` | Path to ffmpeg binary (defaults to `ffmpeg`) | No |
| `POSTGRES_DB` | The name of database to use for Rails (defaults to `alexandria`) | No |
| `POSTGRES_HOST` | The hostname for the postgresql server | Yes |
| `POSTGRES_PASSWORD` | The database password | Yes |
| `POSTGRES_PORT` | The port number for the postgresql server | Yes |
| `POSTGRES_USER` | The database user | Yes |
| `RAILS_DB_POOL_SIZE` | The number of connections to use in the Rails database pool | No |
| `RAILS_DB_TIMEOUT` | Timeout threshold for database connections | No |
| `RAILS_QUEUE` | How Rails should queue background jobs (e.g., `inline`, `async`, `resque`; defaults to Resque) | No |
| `RAILS_SERVE_STATIC_FILES` | Whether the Rails server should handle requests to `public` (should be true when not behind a reverse proxy) | No |
| `REDIS_HOST` | The hostname for the redis server (defaults to `localhost`) | No |
| `REDIS_PORT` | The port number for the redis server (defaults to `6379`) | No |
| `SECRET_KEY_BASE` | Secret key base for the application | Yes |
| `SERVER` | Server targeted by Capistrano deployment | Yes, during deploy |
| `SOLR_CORE` | The name of the solr core to use (defaults to `alexandria`) | No |
| `SOLR_HOST` | The hostname for the solr server (defaults to `localhost`) | No |
| `SOLR_PORT` | The port number for the solr server (defaults to `6379`) | No |

# Ingesting records

See {file:ingesting.md} and DCE’s wiki:
<https://github.com/curationexperts/alexandria-v2/wiki>

# Caveats

* Reindexing all objects (to an empty solr) requires two passes
  (`2.times { ActiveFedora::Base.reindex_everything }`). This
  situtation is not common. The first pass will guarantee that the
  collections are indexed, and the second pass will index the
  collection name on all the objects. The object indexer looks up the
  collection name from solr for speed.

# License

See {file:LICENSE.md}

Some parts of `lib/*vocabularies` and `spec/vocabularies` are licensed
as follows:

```
Copyright 2013 Oregon Digital ( Oregon State University & University of
Oregon )
Copyright 2015 Data Curation Experts

Additional copyright may be held by others, as reflected in the commit log

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
