# frozen_string_literal: true

module CurationConcernsHelper
  include ::BlacklightHelper
  include CurationConcerns::MainAppHelpers

  # @param [SolrDocument, CurationConcerns::FileSetPresenter] doc
  def url_for_document(doc, options = {})
    return unless doc
    # Pull the wrapped solr_doc out of FileSetPresenter
    doc = doc.solr_document if doc.respond_to? :solr_document
    return doc.public_uri if doc.public_uri

    case Array.wrap(doc["has_model_ssim"]).first
    when "Collection"
      collection_path(doc)
    when *CurationConcerns.config.registered_curation_concern_types
      if doc.ark
        ark_path(doc.ark)
      else
        solr_document_path(search_state.url_for_document(doc, options))
      end
    else
      super
    end
  end

  # Blacklight passes the results of {#url_for_document} to
  # +#polymorphic_url+ in some cases; we return a string from that
  # method to force +lib/ark+ links. these two facts are incompatible!
  # +#polymorphic_url+ wants its first argument to be
  # +record_or_hash_or_array+.
  #
  # This is a very heavy handed override seeking to capture cases where
  # the results of {#ark_path} are given to this method. Is there a
  # better fix for this? Yes, certainly. It's to stop returning a String,
  # and change ActiveModel::Name and/or PolymorphicRoutes such that they
  # return the Ark in the correct cases. What will happen to Blacklight
  # and/or to local code if we do this? Let's not find out!
  def polymorphic_url(*args)
    return args.first if args.first.is_a?(String) &&
                         args.first.start_with?("/lib/ark:/")
    super
  end

  # This is required because Blacklight 5.14 uses polymorphic_url
  # in render_link_rel_alternates
  def etd_url(*args)
    solr_document_url(args)
  end

  # This is required because Blacklight 5.14 uses polymorphic_url
  # in render_link_rel_alternates
  def image_url(*args)
    solr_document_url(args)
  end

  # we're using our own helper rather than the generated route helper
  # because the default helper escapes
  # slashes. https://github.com/rails/rails/issues/16058
  def ark_path(ark)
    "/lib/#{ark}"
  end

  ##
  # Get the URL for tracking search sessions across pages using
  # polymorphic routing
  def session_tracking_path(document, params = {})
    return if document.nil?
    blacklight.track_search_context_path(document, params)
  end
end
