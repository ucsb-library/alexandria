FROM ruby:2.3-stretch

RUN echo "deb http://archive.debian.org/debian stretch main" > /etc/apt/sources.list

RUN curl -sL https://deb.nodesource.com/setup_12.x | bash -

RUN apt-get update && \
  apt-get install -y --no-install-recommends \
    build-essential \
    curl \
    git \
    imagemagick \
    libpq-dev \
    libsqlite3-dev \
    libxml2-dev \
    libxslt-dev \
    netcat-openbsd \
    nodejs \
    openjdk-8-jdk \
    unzip

RUN gem i rubygems-update -v '<3' && update_rubygems
RUN gem install bundler:1.17.2 --no-document

RUN mkdir /fits
WORKDIR /fits
RUN curl -sLO https://projects.iq.harvard.edu/files/fits/files/fits-0.8.6_1.zip
RUN unzip fits-0.8.6_1.zip
RUN chmod +x fits-0.8.6/fits.sh
ENV PATH="/fits/fits-0.8.6:$PATH"

RUN mkdir /data
WORKDIR /data

COPY Gemfile* ./
RUN bundle install --jobs "$(nproc)" --verbose

COPY . .

RUN bundle exec rake assets:precompile

EXPOSE 3000
CMD ["bundle", "exec", "puma", "-b", "tcp://0.0.0.0:3000"]
ENTRYPOINT ["/bin/sh", "/data/bin/docker-entrypoint.sh"]
