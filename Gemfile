# frozen_string_literal: true

source "https://rubygems.org"

group :production do
  gem "pg", "~> 0.18.4"
end

gem "active-triples", "~> 0.11.0"
gem "autoprefixer-rails", "< 9" # pinned for rubyracer
gem "blacklight-gallery", "~> 0.8.0"
gem "blacklight_oai_provider"
gem "blacklight_range_limit"
gem "curation_concerns", "~> 1.7.7"
gem "dry-configurable", "< 0.8.3" # pinned for ruby 2.3
gem "dry-equalizer", "< 0.3.0" # pinned for ruby 2.3
gem "dry-inflector", "< 0.2.0" # pinned for ruby 2.3
gem "dry-validation", "< 0.13.0" # pinned for ruby 2.3
gem "ezid-client", "~> 1.2"
gem "font-awesome-sass"
gem "hydra-role-management"
gem "jbuilder", "~> 2.0"
gem "jquery-rails", "~> 4.0"
gem "linked_vocabs",
    git: "https://github.com/samvera-labs/linked_vocabs.git"
gem "marc"
gem "metadata_ci",
    git: "https://github.com/ucsblibrary/metadata-ci.git",
    ref: "0c346700c907f6716971a79b0ec97e23332b2911"
gem "mods", "~> 2.0.3"
gem "net-ldap", "~> 0.14"
gem "nio4r", "< 2.5.3" # pinned for ruby 2.3
gem "nokogiri", "< 1.10" # https://github.com/sul-dlss/mods/issues/33
gem "openseadragon"
gem "puma"
gem "qa", "~> 0.11.0"
gem "rails", "~> 5.1.5"
gem "rdf-marmotta", "~> 0.1.1"
gem "redis-namespace", "< 1.7.0" # pinned for ruby 2.3
gem "resque-pool"
gem "resque-status"
gem "resque-web"
gem "riiif", ">= 2.0.0"
gem "rsolr"
gem "rubyzip", "< 2.0.0" # pinned for ruby 2.3
gem "sass-rails", "~> 5.0"
gem "settingslogic"
gem "signet", "< 0.12" # pinned for ruby 2.3
gem "simple_form", "3.5.0"
gem "therubyracer", "~> 0.12.3", platforms: :ruby
gem "traject", "~> 2.3.2"
gem "uglifier", "~> 3.1"

# https://github.com/amatsuda/kaminari/pull/636
gem "kaminari_route_prefix"

# for bin/ingest
gem "trollop"

# When parsing the ETD metadata file from ProQuest,
# some of the dates are American-style.
gem "american_date", "~> 1.1.0"

group :development, :test do
  # Used exact gem versions for solr_wrapper and fcrepo_wrapper
  # because they aren't careful about making breaking changes on
  # minor releases, so we'll need to be mindful about upgrading.
  gem "fcrepo_wrapper", "0.7.0"
  gem "solr_wrapper", "~> 0.19.0"

  gem "awesome_print"
  gem "bcrypt_pbkdf" # for cap deploy
  gem "byebug", "< 11.1.0" # pinned for ruby 2.3
  gem "ed25519" # for cap deploy
  gem "factory_bot_rails", "< 6.0.0" # pinned for ruby 2.3
  gem "poltergeist"
  gem "rspec-activemodel-mocks"
  gem "rspec-rails"
  gem "rubocop", "< 0.82.0", require: false # pinned for ruby 2.3
  gem "rubocop-rspec", "< 1.39.0", require: false # pinned for ruby 2.3
  gem "spring", "< 2.1.0" # pinned for ruby 2.3
  gem "spring-commands-rspec", group: :development
  gem "sqlite3"
end

group :test do
  gem "capybara", "< 3.16.0" # pinned for ruby 2.3
  gem "ci_reporter"
  gem "database_cleaner"
  gem "rails-controller-testing"
  gem "timecop"
  gem "vcr"
  gem "webmock", require: false
end

group :development do
  gem "capistrano", "~> 3.8.0"
  gem "capistrano-bundler"
  gem "capistrano-passenger"
  gem "capistrano-rails", ">= 1.1.3"
  gem "highline"

  gem "http_logger"
  gem "method_source"
  gem "pry"
  gem "pry-doc"
end
