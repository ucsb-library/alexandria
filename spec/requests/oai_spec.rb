# frozen_string_literal: true

require "rails_helper"

RSpec.describe "Oai requests", type: :request do
  before do
    FactoryBot.create(:image,
                      visibility: 'open',
                      created_attributes: [{ start: ["1925-11"] }],
                      issued_attributes: [{ start: ["2021"] }])
  end

  describe 'metadataPrefix=oai_dpla' do
    it "has oai_dpla as a metadata type" do
      get "/catalog/oai?verb=ListMetadataFormats"

      expect(response.body).to match(/oai_dpla/)
    end

    it "gives valid XML" do
      get "/catalog/oai?verb=ListRecords&metadataPrefix=oai_dpla"

      oai_xml = Nokogiri::XML(response.body)
      oai_xml.validate

      expect(oai_xml.errors).to be_empty
    end

    it "includes dct:created and dct:issued" do
      get "/catalog/oai?verb=ListRecords&metadataPrefix=oai_dpla"

      expect(response).to be_success
      expect(response.body).to match(%r{dct:created})
      expect(response.body).to match(%r{dct:issued})
    end

    it "includes edm:isShownAt" do
      get "/catalog/oai?verb=ListRecords&metadataPrefix=oai_dpla"

      expect(response).to be_success
      expect(response.body).to match(%r{edm:isShownAt})
    end

    it "includes edm:object" do
      get "/catalog/oai?verb=ListRecords&metadataPrefix=oai_dpla"

      expect(response).to be_success
      expect(response.body).to match(%r{edm:object})
    end
  end

  it "renders the basic OAI response" do
    get "/catalog/oai?verb=Identify"
    expect(response).to be_success
    expect(response.body).to match(%r{https:\/\/alexandria.ucsb.edu\/catalog\/oai})
  end

  it "returns valid XML" do
    get "/catalog/oai"
    oai_xml = Nokogiri::XML(response.body)
    oai_xml.validate
    expect(oai_xml.errors).to eq([])
  end

  it "has dc as a metadata type" do
    get "/catalog/oai?verb=ListMetadataFormats"
    expect(response.body).to match(/oai_dc/)
  end

  it "has a record in the repo" do
    get "/catalog/oai?verb=ListRecords&metadataPrefix=oai_dc"
    expect(response.body).to match(%r{<identifier>oai:ucsb:.*<\/identifier>})
  end

  it "has a record in the repo with a title" do
    get "/catalog/oai?verb=ListRecords&metadataPrefix=oai_dc"
    expect(response.body).to match(%r{<dc:title>.*<\/dc:title>})
  end

  it "has a record in the repo with a dc identifier with an http prefix" do
    get "/catalog/oai?verb=ListRecords&metadataPrefix=oai_dc"
    prefix_id = %r{<dc:identifier>https://alexandria.ucsb.edu/lib/.*<\/dc:identifier>}
    expect(response.body).to match(prefix_id)
  end
end
