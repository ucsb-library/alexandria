class DplaOai < OAI::Provider::Metadata::DublinCore
  def initialize
    super
    @prefix = 'oai_dpla'
  end
end
